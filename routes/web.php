<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// Enable Cors
use App\Helpers\JsonApiResponse;

$router->options('{route:.*}', function () {
    return response('', 204, [
        'Access-Control-Allow-Origin'  => '*',
        'Access-Control-Allow-Methods' => 'POST, GET, PUT, OPTIONS',
        'Access-Control-Max-Age'       => '3600',
        'Access-Control-Allow-Headers' => 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With',
    ]);
});

$router->get('/', function () use ($router) {
    return JsonApiResponse::success([
        'name'     => "Rport on Axis",
        'repo_url' => 'https://bitbucket.org/cloudradar/rport-on-axis/src/master/',
        'built_on' => $router->app->version(),
    ]);
});

$router->group(['prefix' => 'cameras', 'middleware' => 'auth.jwt'], function () use ($router) {
    $router->get('/', 'CameraController@list');
    $router->get('/{cameraId}', 'CameraController@show');
    $router->get('/{cameraId}/tunnels', 'CameraController@showTunnels');
    $router->put('/{cameraId}/expose', 'CameraController@expose');
    $router->delete('/{cameraId}/expose', 'CameraController@unexpose');
});

$router->get('/heartbeats/{exposalId}', 'HeartbeatController@store');

$router->group(['prefix' => 'token', 'middleware' => 'auth.basic'], function () use ($router) {
    $router->get('/', 'TokenController@create');
    $router->post('/', 'TokenController@create');
});


$router->get('/status', [
    'middleware' => 'auth.jwt',
    'uses'       => 'StatusController@show',
]);


if (env('APP_DEBUG')) {
    /**
     * This controller is for devolping and debugging only.
     * Caddy proxies are managed by the camera tunnels. Manual management is not allowed.
     */
    $router->group(['prefix' => 'caddy-proxies', 'middleware' => 'auth.basic'], function () use ($router) {
        $router->get('/', 'CaddyProxyController@list');
        $router->put('/{id}', 'CaddyProxyController@create');
        $router->get('/{id}', 'CaddyProxyController@show');
        $router->delete('/{id}', 'CaddyProxyController@delete');
    });
}
