# Rport for Axis Cameras

## Goal 
This rpository hold all needed information and the source code a wrapper API to securly access Axis cameras over the internet.

The API makes any camera available on a unique but public hostname using the rport tunnels behind the scenes. 

The camera credentials are stored inside the rport vault. Authentication headers are injected by the proxy on the fly. 
They will never be visible to the user. This allows the direct integration of the camera interface into publically accesible web frontend.

## Prerequisits 
To run the proxy you need a server with
* a running RPort server
* a running Caddy server
* PHP 8.1 installed
* a wildcard SSL certificate
* a wildcard DNS A record

## Usage exmaples

### Authentication
For all requests a JWT is required, except on `/token` where basic auth is used.

The JWT can also be generated elsewhere as long as the generator and the wrapper api share the same JWT secret. 

To get a token
`curl -s http://localhost:8080/token -u admin:admin` 
returns
```json
{
    "data": {
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vZXhhbXBsZS5vcmciLCJhdWQiOiJodHRwOi8vZXhhbXBsZS5jb20iLCJpYXQiOjEzNTY5OTk1MjQsIm5iZiI6MTM1NzAwMDAwMH0.rdk8rVbF_X7GEYeQyiSRgwLi_rqy2po-yHy05Pn6m-s"
  },
  "meta": null
}
```

A token can be requested using POST or GET. The /token endpoint accepts the following optional parameters:

* `camera_id`: If set, an exposal can only be requested for this camera. **If not set, access to all cameras is granted.**
* `camera_user`: Use the specified usernamer instead of the default `DEFAULT_CAMERA_USER` from `.env`
* `camera_password`: Use the specified password instead of the default `DEFAULT_CAMERA_PASSWORD` from `.env`

Parameter can send as follows:

* URL encoded
    ```
    curl -s ${API_URL}"/token?camera_user=bunny&camera_password=foobaz" -u admin:${PASSWORD}
    ```
* Using form-data
    ```
    curl -s ${API_URL}/token -u admin:${PASSWORD} \
    -F "camera_user=rabbit" \
    -F "camera_password=12345" \
    -F "camera_id=456789" 
    ``` 
* Using Json data
    ```
    curl -s ${API_URL}/token --data-raw 
    '{ 
        "camera_id":"2334tg",
        "camera_user":"user1",
        "camera_password":"secret1"
    }' \
    -H "content-type:application/json" -u admin:${PASSWORD}
    ``` 

Test the token
```bash
TOKEN=<YOUR_TOKEN>
curl -s -H "Authorization: Bearer $TOKEN" http://localhost:8080/status
```
returns the decoded and decrypted payload of the JWT, for example
````json
{
    "data": {
        "_token": "Zz1FRKDtb7JaFgriWsUvUI1pJaUwYlm5JLWD1lw3",
        "cameraId": "e68f0357ee8142eda0a24f923d9373c4",
        "cameraUser": "rabbit",
        "cameraPassword": "12345"
    }
}
````

### Cameras
List connected cameras
```bash
curl -s -H "Authorization: Bearer $TOKEN" http://localhost:8080/cameras
```
returns for example
````json
{
  "data": [
    {
      "id": "0027cd9e353f4b609d48b62cec28abdc",
      "name": "cam1",
      "hostname": "odroid",
      "tags": [
        "Germany",
        "Cologne"
      ],
      "connection_state": "connected"
    },
    {
      "id": "221cefce4b3e4ee290f2eb69d37c948e",
      "name": "demo-cam1",
      "hostname": "axis-accc8ec3d337",
      "tags": [
        "axis"
      ],
      "connection_state": "connected"
    },
    {
      "id": "e9fe82d4d038425a8cf8a37e7a1fe18a",
      "name": "cameras-tinyserver-net",
      "hostname": "cameras-tinyserver-net",
      "tags": [
        "Germany",
        "Munich"
      ],
      "connection_state": "connected"
    }
  ],
  "meta": null
}
````

> This is an unfiltered list of all clients connected to rport.
>
> @Tbd: Just return devices that are Axis cameras? 

Expose the camera to the internet via a proxy
```bash
CAMERA_ID=221cefce4b3e4ee290f2eb69d37c948e
curl -s -H "Authorization: Bearer $TOKEN" http://localhost:8080/cameras/${CAMERA_ID}/expose -X PUT
```
returns the URL to access the camera.
````json
{
  "data": {
    "exposalId": "947c7dae-b841-4633-ba62-33c23e71b507",
    "exposedUrl": "https://947c7dae-b841-4633-ba62-33c23e71b507.cameras.tinyserver.net",
    "heartbeatUrl": "http://localhost:8080/heartbeats/947c7dae-b841-4633-ba62-33c23e71b507"
  },
  "meta": null
}
````

To unexpose a camera
````bash
curl -s -H "Authorization: Bearer $TOKEN" http://localhost:8080/cameras/${CAMERA_ID}/expose -X DELETE
````
The url of the camera will return 404 immediately and the tunnel is closed.

### For testing only
Requires the app to run in debug mode. 

List proxy instances
```bash
curl -s localhost:8080/caddy-proxies/|jq
curl -s localhost:8080/caddy-proxies/|jq .data[] -r|grep '@id'
```

Delete an instance
```bash
ID='cam-6d282a47-18e5-4e0f-91ff-5eaac2cc0327'
curl -s -H "content-type: application/json" localhost:8080/caddy-proxies/${ID} -X DELETE|jq
```

## Installation
### Wrapper API
You need PHP 8.1 on your system. *Below instructions are for Debian 11 (Bullseye) only.*

    sudo wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg 
    sudo sh -c 'echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list'
    sudo apt update
    sudo apt install php8.1-fpm php8.1-curl git beanstalkd
    curl -sS https://getcomposer.org/installer | php
    sudo mv composer.phar /usr/local/bin/composer

Change to the `www-data` account and intsall the wrapper API.

    root@server:~# chown www-data:www-data /var/www
    root@server:~# su - www-data -s /bin/bash
    
    www-data@server:~$ cd /var/www
    www-data@server:~$ git clone https://cloudradar@bitbucket.org/cloudradar/rport-on-axis.git
    www-data@server:~$ cd rport-on-axis/
    www-data@server:~$ composer install
    www-data@server:~$ cp .env.example .env

Create a random 32bit APP_KEY with `openssl rand -hex 16` and enter it to the `.env`.

Now edit the `.env` file and enter your settings.

The API relies on asynchronous data processing. Therefore, a queue worker process and the heartbeat must be started.
Register the systemd services from the root account.
    
    ln -s /var/www/rport-on-axis/rport-on-axis-queue.service /etc/systemd/system/
    systemctl start rport-on-axis-queue
    systemctl enable rport-on-axis-queue
    ln -s /var/www/rport-on-axis/rport-on-axis-heartbeat.service /etc/systemd/system/
    systemctl start rport-on-axis-heartbeat
    systemctl enable rport-on-axis-heartbeat

### Wildcard DNS A record

Go to your DNS and create DNS A wildcard record for the subdomain you want to use. For example `*.cameras.example.com`.

### Wildcard SSL Certificate
Because the API creates random subdomains on the fly, a wildcard certificate is needed for a given base domains.
For example, you want expose the cameras on {random}.cameras.example.com you need a wildcard certificate for `*.cameras.example.com`.

Let's encrypt and `certbot` support creating wild card certificates, but the so-called [DNS confirmation](https://letsencrypt.org/docs/challenge-types/#dns-01-challenge) is required.

The following exmaple demonstrates, how to request the certificate.

    certbot certonly --manual --preferred-challenges dns -d cameras.example.com -d *.cameras.example.com

Before executing the command, log in to your DNS configuration.
Follow the instruction on the screen and create the requested TXT records on your DNS.
The command seems to hang, but it's just waiting for the DNS to be created. 

### Caddy
You need Caddy v2 installed. [Read more](https://caddyserver.com/docs/install#debian-ubuntu-raspbian)

Run caddy with the following configuration.

```text
# Global Settings
{
    grace_period 1s # crucial setting, otherwise caddy waits for all http connections to be terminated before reloading the configuration
    auto_https off
    default_sni cameras.<your-host-name>
}

# Pass API rqeuest to PHP
https://api.<your-host-name> {
  tls /etc/letsencrypt/live/<your-host-name>/fullchain.pem /etc/letsencrypt/live/<your-host-name>/privkey.pem
  root * /var/www/rport-on-axis/public
  php_fastcgi unix//var/run/php/php8.1-fpm.sock
  try_files {path} /index.php
  file_server
  encode zstd gzip
  log {
	output file /tmp/api.<your-host-name>.log
  }
}

# Default virtual host. If no other configuration catches the request, this one returns 404
https:// {
    tls /etc/letsencrypt/live/<your-host-name>/fullchain.pem /etc/letsencrypt/live/<your-host-name>/privkey.pem
    respond "not found" 404
}
```
## Update
To update the wrapper API from the git repository:

1. From the root account, stop the services
    ```
    systemctl stop rport-on-axis-queue
    systemctl stop rport-on-axis-heartbeat
    ``` 
2. From the www-data account, pull changes
    ```
    cd /var/www/rport-on-axis
    git pull
    composer install
    ```   
3. From the root account, start the services again
   ```
   systemctl start rport-on-axis-queue
   systemctl start rport-on-axis-heartbeat
   ```

You can execute the above commands in a single run from the root account

```bash
systemctl stop rport-on-axis-queue
systemctl stop rport-on-axis-heartbeat
cat <<EOF|sudo -u www-data bash
cd /var/www/rport-on-axis
git pull
composer install
EOF
systemctl start rport-on-axis-queue
systemctl start rport-on-axis-heartbeat
```

## Observer the logs
Logs are in `/var/www/rport-on-axis/storage/logs/app.log`.

If the log is too verbose, lower the log level to `INFO` in the `.env`.

After changing the `.env` the service must be restarted.
