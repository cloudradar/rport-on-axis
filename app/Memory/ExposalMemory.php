<?php

namespace App\Memory;

use App\Data\ExposalData;
use App\Helpers\Jsonfile;
use Illuminate\Support\Collection;


class ExposalMemory
{
    public const FILE_PREFIX = 'rport-camera-exposal-';

    /**
     * Store the details of a camera exposal into the default cache storage.
     * @param string $exposalId
     * @param string $cameraId
     * @param int $tunnelId
     * @param string $hostname
     * @return void
     */
    public static function store(ExposalData $exposal): void
    {
        Jsonfile::put(self::jsonfile($exposal->cameraId), $exposal->toArray());
    }

    public static function get(string $cameraId): ?ExposalData
    {
        $file = self::jsonfile($cameraId);
        if (!file_exists($file)) {
            return null;
        }

        return new ExposalData(Jsonfile::get($file));
    }

    public static function getAll(): Collection
    {
        $exposals = collect();
        foreach (scandir(env('MEMORY_DIR', '/tmp')) as $file) {
            if (!str_ends_with($file, '.json')) continue;
            if (!str_starts_with($file, self::FILE_PREFIX)) continue;
            $file = env('MEMORY_DIR', '/tmp') . '/' . $file;
            $exposals->push(new ExposalData(Jsonfile::get($file)));
        }

        return $exposals;
    }

    public static function delete(string $cameraId): void
    {
        unlink(self::jsonfile($cameraId));
    }

    private static function jsonfile(string $name): string
    {
        return env('MEMORY_DIR', '/tmp') . '/' . self::FILE_PREFIX . $name . '.json';
    }
}
