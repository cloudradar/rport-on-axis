<?php

namespace App\Jobs;

use App\Clients\CaddyProxyClient;
use Illuminate\Support\Facades\Log;

class CaddyDeleteRouteJob extends Job
{
    public function __construct(private string $exposalId)
    {
        Log::debug('CaddyDeleteRouteJob dispatched');
    }

    public function handle()
    {
        try {
            (new CaddyProxyClient())->proxyRouteId($this->exposalId)->delete();
        } catch (\Exception $e) {
            Log::error('Deleting proxy route on caddy failed', [$e->getMessage()]);
        }
        Log::debug('CaddyDeleteRouteJob processed');
    }
}
