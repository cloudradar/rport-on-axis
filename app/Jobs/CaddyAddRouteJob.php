<?php

namespace App\Jobs;

use App\Clients\CaddyProxyClient;
use App\Data\CaddyProxyRoute;
use App\Data\ExposalData;
use Illuminate\Support\Facades\Log;

class CaddyAddRouteJob extends Job
{

    public function __construct(private ExposalData $exposalData)
    {
        Log::debug('CaddyAddRouteJob dispatched', $this->exposalData->toArray());
    }

    public function handle()
    {
        $route = new CaddyProxyRoute($this->exposalData->exposalId);
        $route->routeHostname($this->exposalData->hostname)
              ->toTunnelPort($this->exposalData->lport)
              ->withBasicAuth($this->exposalData->authUsername, $this->exposalData->authPassword)
              ->withHeader('Host', 'localhost-basic');
        (new CaddyProxyClient())->proxyRoute($route)->put();
        Log::debug('CaddyAddRouteJob processed');
    }
}
