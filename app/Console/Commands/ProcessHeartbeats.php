<?php

namespace App\Console\Commands;

use App\Actions\UnexposeCameraAction;
use App\Data\ExposalData;
use App\Memory\ExposalMemory;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Symfony\Component\Console\Command\SignalableCommandInterface;

class ProcessHeartbeats extends Command implements SignalableCommandInterface
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'heartbeats:process {--once}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Stop proxies and tunnels without active watchers';
    private bool $sigint = false;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function getSubscribedSignals(): array
    {
        return [SIGINT, SIGTERM];
    }

    public function handleSignal(int $signal): void
    {
        if ($signal === SIGINT or $signal === SIGTERM) {
            Log::debug(__CLASS__ . ': Caught signal. Will stop now.', ['signal' => $signal]);
            $this->sigint = true;
        }
    }

    public function handle(UnexposeCameraAction $unexposeCameraAction)
    {
        while (true) {
            $this->process($unexposeCameraAction);
            if ($this->option('once') or $this->sigint) break;
            sleep(5);
        }
    }

    /**
     * Execute the console command.
     *
     */
    private function process(UnexposeCameraAction $unexposeCameraAction)
    {
        // Fetch the current active exposals
        $exposals = ExposalMemory::getAll();
        Log::debug(__CLASS__ . sprintf(': %d exposals active.', $exposals->count()));
        $exposals->each(function ($exposal) use ($unexposeCameraAction) {
            /** @var ExposalData $exposal */
            Log::debug(__CLASS__ . ': Verifying heartbeat', ['cameraID' => $exposal->cameraId, 'exposalId' => $exposal->exposalId]);
            if ($heartbeat = cache::get($exposal->exposalId)) {
                Log::info(__CLASS__ . sprintf(': Camera %s: Last heartebeat received %d seconds ago.', $exposal->cameraId, time() - $heartbeat));

                return;
            }
            Log:
            info(__CLASS__ . sprintf(': Camera %s has no heartbeat. Will unexpose.', $exposal->cameraId));
            $unexposeCameraAction->execute($exposal);
        });
    }
}
