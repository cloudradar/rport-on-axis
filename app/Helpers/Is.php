<?php

namespace App\Helpers;

class Is
{
    public static function uuid(string $input):bool
    {
        return (bool) preg_match("/^[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12}$/",$input);
    }
}
