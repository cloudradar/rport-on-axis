<?php

namespace App\Helpers;

use App\Data\ExposalData;
use App\Jobs\CaddyDeleteRouteJob;
use App\Memory\ExposalMemory;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class ExposalStatus
{
    public static function isExposed(?ExposalData $exposal): string|bool
    {
        if (is_null($exposal)) return false;

        if (200 == $httpStatus = Http::timeout(3)->head('https://' . $exposal->hostname)->status()) {
            return $exposal->hostname;
        }
        Log::info(sprintf('Found orpahned exposal memory. %s is no longer active. Got HTTP status %s.', $exposal->hostname, $httpStatus));
        // Clean up the orpahned data
        dispatch(new CaddyDeleteRouteJob($exposal->exposalId));
        ExposalMemory::delete($exposal->cameraId);

        return false;
    }

}
