<?php

namespace App\Helpers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;

class JsonApiResponse
{
    public static function success(array $data)
    {
        return new JsonResponse(['data' => $data], 200, self::headers());
    }

    public static function empty()
    {
        return new Response('', 204, self::headers());
    }

    public static function error(string|array $error, int $statusCode = 400)
    {
        return new JsonResponse([
            'error'       => $error,
            'status_code' => $statusCode,
        ], $statusCode, self::headers());
    }

    private static function headers()
    {
        return [
            'Access-Control-Allow-Origin'  => '*',
            'Access-Control-Allow-Methods' => 'POST, GET, PUT, OPTIONS',
            'Access-Control-Allow-Headers' => 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With',
        ];
    }
}
