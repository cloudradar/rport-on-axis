<?php

namespace App\Helpers;

class Jsonfile
{
    public static function put(string $filename, array $data): void
    {
        file_put_contents($filename, json_encode($data));
    }

    public static function get(string $filename): array
    {
        return json_decode(file_get_contents($filename), true);
    }
}
