<?php

namespace App\Http\Controllers;

use App\Helpers\JsonApiResponse;
use Illuminate\Http\Request;

class StatusController extends Controller
{

    public function show(Request $request)
    {
        return JsonApiResponse::success($request->session()->all());
    }
}
