<?php

namespace App\Http\Controllers;

use App\Helpers\JsonApiResponse;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;

class TokenController extends Controller
{
    public function create(Request $request)
    {
        $payload = [
            'camera_id'       => $request->input('camera_id'),
            'camera_user'     => $request->input('camera_user'),
            'camera_password' => $request->input('camera_password'),
        ];
        Log::debug('Creating JWT', ['payload' => $payload]);
        $jwt = JWT::encode(Crypt::encryptString(json_encode($payload)), env('JWT_SECRET'), 'HS256');

        return JsonApiResponse::success(['token' => $jwt]);
    }
}
