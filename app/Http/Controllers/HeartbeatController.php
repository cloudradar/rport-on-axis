<?php

namespace App\Http\Controllers;

use App\Helpers\Is;
use App\Helpers\JsonApiResponse;
use Illuminate\Support\Facades\Cache;

class HeartbeatController extends Controller
{
    public function store(string $exposalId)
    {
        if (Is::uuid($exposalId)) {
            Cache::put($exposalId, time(), 30);

            return JsonApiResponse::empty();
        }

        return JsonApiResponse::error('Invalid Expsoal UUID');
    }
}
