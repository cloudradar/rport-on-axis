<?php

namespace App\Http\Controllers;

use App\Clients\CaddyProxyClient;
use App\Data\CaddyProxyRoute;

class CaddyProxyController extends Controller
{
    public function list(CaddyProxyClient $proxies): array
    {
        return [
            'data' => $proxies->get(),
            'meta' => null,
        ];
    }

    public function show(string $id): array
    {
        return [
            'data' => (new CaddyProxyClient($id))->get(),
            'meta' => null,
        ];
    }

    public function delete(string $id): array
    {
        (new CaddyProxyClient($id))->delete();

        return [];
    }

    public function create($id): array
    {
        $route = new CaddyProxyRoute($id);
        $route->routeHostname(sprintf('cam%s.cameras.tinyserver.net', $id))
              ->toTunnelPort(28649)
              ->withBasicAuth('admin', 'foobaz');
        (new CaddyProxyClient())->proxyRoute($route)->put();

        return [];
    }
}
