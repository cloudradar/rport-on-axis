<?php

namespace App\Http\Controllers;

use App\Actions\ExposeCameraAction;
use App\Actions\GetCameraAction;
use App\Actions\GetCamerasAction;
use App\Actions\UnexposeCameraAction;
use App\Clients\RportCamera;
use App\Data\CameraData;
use App\Helpers\JsonApiResponse;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class CameraController extends Controller
{
    public function list(GetCamerasAction $getCamerasAction, Request $request)
    {
        if ($request->session()->get('cameraId')) {
            return JsonApiResponse::error('Session limited to a single camera id', 403);
        }
        $cameras = $getCamerasAction->execute()->map(function ($camera) {
            /**
             * @var CameraData $camera
             */
            return [
                'id'               => $camera->id,
                'name'             => $camera->name,
                'hostname'         => $camera->hostname,
                'tags'             => $camera->tags,
                'connection_state' => $camera->connectionState,
            ];
        });

        return JsonApiResponse::success($cameras->toArray());
    }

    public function show(string $cameraId, GetCameraAction $getCameraAction)
    {
        $camera = $getCameraAction->execute($cameraId);

        return JsonApiResponse::success([
            'id'               => $camera->id,
            'name'             => $camera->name,
            'tags'             => $camera->tags,
            'connection_state' => $camera->connectionState,
            'tunnels'          => $camera->tunnels,
        ]);
    }

    public function showTunnels(string $cameraId)
    {
        $tunnels = (new RportCamera($cameraId))->getTunnels();
        return JsonApiResponse::success($tunnels->toArray());
    }

    public function expose(string $cameraId, ExposeCameraAction $exposeCameraAction, Request $request)
    {
        if ($request->session()->get('camera_id') && $cameraId != $request->session()->get('camera_id')) {
            return JsonApiResponse::error('You are not allowed to expose this camera', 403);
        }
        Log::info('Exposing camera ' . $cameraId);

        $exposal = $exposeCameraAction->execute($cameraId, $request->session()->get('cameraUser'), $request->session()->get('cameraPassword'));

        return JsonApiResponse::success([
            'exposal_id'    => $exposal->exposalId,
            'exposed_url'   => 'https://' . $exposal->hostname,
            'heartbeat_url' => url('/heartbeats/' . $exposal->exposalId),
        ]);
    }

    public function unexpose(string $cameraId, UnexposeCameraAction $unexposeCameraAction)
    {
        $unexposeCameraAction->executeFromController($cameraId);

        return JsonApiResponse::empty();
    }
}
