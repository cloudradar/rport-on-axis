<?php

namespace App\Http\Middleware;

use App\Helpers\JsonApiResponse;
use Closure;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;

class JwtAuthentication
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$jwt = $request->header('Authorization')) {
            return JsonApiResponse::error('Authorization header missing', 401);
        }
        $jwt = str_ireplace('Bearer ', '', $jwt, $count);
        if (!(bool)$count) {
            return JsonApiResponse::error('Authorization Bearer header missing', 401);
        }
        try {
            $decoded = JWT::decode($jwt, new Key(env('JWT_SECRET'), 'HS256'));
        } catch (\Exception) {
            return JsonApiResponse::error('Bad token', 401);
        }
        $decrypted = json_decode(Crypt::decryptString($decoded), true);
        Log::debug('Decrypted JWT payload',['payload'=>$decrypted]);
        $request->session()->put('cameraId', Arr::get($decrypted, 'camera_id'));
        $request->session()->put('cameraUser', Arr::get($decrypted, 'camera_user'));
        $request->session()->put('cameraPassword', Arr::get($decrypted, 'camera_password'));

        return $next($request);
    }
}
