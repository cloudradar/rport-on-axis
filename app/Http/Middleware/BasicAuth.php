<?php

namespace App\Http\Middleware;

use App\Helpers\JsonApiResponse;
use Closure;

class BasicAuth
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( env('BASIC_AUTH_USER','admin') !== $request->getUser() or
            env('BASIC_AUTH_PASSWORD','gaer5ohLeer4uil6qu') !== $request->getPassword()
        ) {
            return JsonApiResponse::error('Unauthorized', 401);
        }

        return $next($request);
    }
}
