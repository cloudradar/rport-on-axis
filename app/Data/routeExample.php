<?php

$jayParsedAry = [
    "@id" => "cam1",
    "handle" => [
        [
            "handler" => "subroute",
            "routes" => [
                [
                    "handle" => [
                        [
                            "body" => "Access denied",
                            "close" => true,
                            "handler" => "static_response",
                            "status_code" => 403
                        ]
                    ],
                    "match" => [
                        [
                            "path" => [
                                "/control.php",
                                "/admin*"
                            ]
                        ]
                    ]
                ],
                [
                    "handle" => [
                        [
                            "handler" => "reverse_proxy",
                            "headers" => [
                                "request" => [
                                    "set" => [
                                        "Host" => [
                                            "{http.reverse_proxy.upstream.hostport}"
                                        ],
                                        "X-Forwarded-Host" => [
                                            "{http.request.host}"
                                        ],
                                        "Authorization" => [
                                            "Basic YWRtaW46Zm9vYmF6"
                                        ]
                                    ]
                                ]
                            ],
                            "upstreams" => [
                                [
                                    "dial" => "127.0.0.1:26623"
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]
    ],
    "match" => [
        [
            "host" => [
                "cam1.cameras.tinyserver.net"
            ]
        ]
    ],
    "terminal" => true
];
