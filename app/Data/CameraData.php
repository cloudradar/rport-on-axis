<?php

namespace App\Data;

use Illuminate\Support\Arr;
use Spatie\DataTransferObject\DataTransferObject;

class CameraData extends DataTransferObject
{
    public string $id;
    public string $name;
    public string $hostname;
    public array $tags;
    /* @var string $connectionState either 'connected' or 'disconnected' */
    public string $connectionState;
    public array $tunnels;

    public static function create(array $camera): self
    {
        return new self([
            'name'            => $camera['name'],
            'hostname'        => $camera['hostname'],
            'id'              => $camera['id'],
            'tags'            => Arr::get($camera,'tags',[]),
            'connectionState' => Arr::get($camera,'connection_state','unknown'),
            'tunnels'         => Arr::get($camera,'tunnels',[]),
        ]);
    }
}
