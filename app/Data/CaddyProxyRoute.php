<?php

namespace App\Data;

class CaddyProxyRoute
{
    private string $hostName;
    private int $tunnelPort;
    private array $headers = [
        "X-Forwarded-Host" => [
            "{http.request.host}",
        ],
    ];

    public function __construct(private string $id)
    {

    }

    public function routeHostname(string $hostName): self
    {
        $this->hostName = $hostName;

        return $this;
    }

    public function toTunnelPort(int $tunnelPort): self
    {
        $this->tunnelPort = $tunnelPort;

        return $this;
    }

    public function withBasicAuth(string $username, string $password): self
    {
        $auth = 'Basic ' . base64_encode(sprintf("%s:%s", $username, $password));
        $this->headers = array_merge($this->headers, ["Authorization" => [$auth]]);

        return $this;
    }

    public function withHeader(string $key, string $value):self
    {
        $this->headers = array_merge($this->headers, [trim($key) => [trim($value)]]);
        return $this;
    }

    public function export()
    {
        return [
            "@id"      => $this->id,
            "handle"   => [
                [
                    "handler" => "subroute",
                    "routes"  => [
                        [
                            "handle" => [
                                [
                                    "body"        => "Access denied",
                                    "close"       => true,
                                    "handler"     => "static_response",
                                    "status_code" => 403,
                                ],
                            ],
                            "match"  => [
                                [
                                    "path" => [
                                        "/control.php",
                                        "*devicemgmt.wsdl*",
                                        "*pwdgrp.cgi",
                                    ],
                                ],
                            ],
                        ],
                        [
                            "handle" => [
                                [
                                    "handler"   => "reverse_proxy",
                                    "headers"   => [
                                        "request" => [
                                            "set" => $this->headers,
                                        ],
                                    ],
                                    "upstreams" => [
                                        [
                                            "dial" => "127.0.0.1:" . $this->tunnelPort,
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
            "match"    => [
                [
                    // Route applies ony of headers match. aka virtual host
                    "host" => [$this->hostName],
                ],
            ],
            "terminal" => true,
        ];
    }
}
