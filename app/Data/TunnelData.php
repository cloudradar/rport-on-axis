<?php

namespace App\Data;

use Spatie\DataTransferObject\DataTransferObject;

class TunnelData extends DataTransferObject
{
    public int $id;

    /* @var int $lport Port of the tunnel on the rport server */
    public int $lport;
}
