<?php

namespace App\Data;

use Spatie\DataTransferObject\DataTransferObject;

class ExposalData extends DataTransferObject
{
    public string $exposalId;
    public string $cameraId;
    public string $hostname;
    public int $createTimestamp;
    public int $tunnelId;
    public int $lport;
    /** @var string|null Username for basic auth header */
    public ?string $authUsername = null;
    /** @var string|null Password for basic auth header */
    public ?string $authPassword = null;
}
