<?php

namespace App\Clients;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpKernel\Exception\HttpException;

abstract class JsonApiClient
{
    protected Client $client;
    protected string $uri;
    protected string $baseUri;
    protected ?array $payload = null;
    protected ?array $query = null;

    protected function executeRequest(string $method = 'get'): ?array
    {
        $fullUri = $this->baseUri . $this->uri;
        Log::debug("Sending request to " . $fullUri, ['method' => $method]);
        try {
            switch ($method) {
                case 'get':
                    $response = $this->client->get($this->uri);
                    break;
                case 'put' && $this->payload:
                    $response = $this->client->put($this->uri, ['json' => $this->payload]);
                    break;
                case 'put' && $this->query:
                    $response = $this->client->put($this->uri, ['query' => $this->query]);
                    break;
                case 'put':
                    $response = $this->client->put($this->uri);
                    break;
                case 'delete':
                    $response = $this->client->delete($this->uri);
                    break;
            }

        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                throw new HttpException((int)$e->getResponse()->getStatusCode(), (string)$e->getResponse()->getBody());
            }
            Log::error('Error occurred in get request.', ['error' => $e->getMessage(), 'request' => $e->getRequest()]);
            throw new HttpException(400, 'RPort API request failed: ' . $e->getMessage());
        } catch (\Exception $e) {
            Log::error('RPort API request failed: ' . $e->getMessage());
            throw new HttpException(400, 'unknown error');
        }
        $body = (string)$response->getBody();
        $body = json_decode($body, true) ?: $body; // Try to decode json
        $body = empty($body) ? null : $body; // Set an empty boddy to null
        Log::debug("Got response from " . env('RPORT_API_URL'), [$method, $response->getStatusCode(), $body]);

        return Arr::get($body, 'data', $body);
    }
}
