<?php

namespace App\Clients;

use App\Data\CaddyProxyRoute;
use GuzzleHttp\Client;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CaddyProxyClient extends JsonApiClient
{
    public function __construct(?string $id = null)
    {
        $this->baseUri = env('CADDY_API_URL', 'http://127.0.0.1:2019');
        $this->client = new Client([
            'base_uri' => $this->baseUri,
            'timeout'  => 2.0,
            'headers'  => [
                'Content-Type' => 'application/json',
            ],
        ]);
        $this->uri = '/config/apps/http/servers/srv0/routes';
        if ($id) {
            $this->uri = '/id/' . $id;
        }
    }

    /**
     * Point the client to the route definition of an existing web server.
     * @param CaddyProxyRoute $route
     * @return $this
     */
    public function proxyRoute(CaddyProxyRoute $route): self
    {
        $this->uri .= '/0';
        $this->payload = $route->export();
        $this->uriSupports = ['put'];

        return $this;
    }

    /**
     * Point the client to any named definition.
     * https://caddyserver.com/docs/api#using-id-in-json
     * @param string $id
     * @return $this
     */
    public function proxyRouteId(string $id): self
    {
        $this->uri = '/id/' . $id;
        $this->uriSupports = ['get', 'delete'];

        return $this;
    }

    public function get(): ?array
    {
        try {
            return $this->executeRequest();
        } catch (HttpException $e) {
            // Translate Caddy exceptions into REST standards
            if ($e->getStatusCode() === 500 && preg_match("/unknown object ID '(.*)'/", $e->getMessage(), $match)) {
                throw new HttpException(404, $match[0]);
            }
            // Forward other expections without manipulation
            throw new HttpException($e->getStatusCode(), $e->getMessage());
        }
    }

    public function delete(): void
    {
        $this->executeRequest('delete');
    }

    public function put(): ?array
    {
        return $this->executeRequest('put');
    }

}
