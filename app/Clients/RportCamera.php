<?php

namespace App\Clients;

use App\Data\CameraData;
use App\Data\TunnelData;
use Illuminate\Support\Collection;

class RportCamera extends RportCameras
{
    public function __construct(private string $cameraId)
    {
        parent::__construct();
        $this->uri = 'clients/' . $cameraId;
    }

    public function get(): CameraData
    {

        return CameraData::create($this->executeRequest());
    }

    public function createTunnel(array $params, ?int $overwrite = null): TunnelData
    {
        if ($overwrite) {
            $this->deleteTunnel($overwrite);
        }
        $this->uri = 'clients/' . $this->cameraId . '/tunnels';
        $this->query = $params;

        return new TunnelData($this->executeRequest('put', $params));
    }

    public function deleteTunnel(int $tunnelId): void
    {
        $this->uri = 'clients/' . $this->cameraId . '/tunnels/' . $tunnelId . '?force=true';
        $this->executeRequest('delete');
    }

    /**
     * Point the client to the API URL to the active tunnels of a client aka camera.
     * Used to fetch active tunnels.
     * @return Collection
     */
    public function getTunnels(): Collection
    {
        $this->uri = 'clients?fields[clients]=tunnels';

        return collect($this->executeRequest('get')[0]['tunnels']);
    }
}

