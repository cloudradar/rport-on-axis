<?php

namespace App\Clients;

use App\Data\CameraData;
use App\Data\TunnelData;
use GuzzleHttp\Client;
use Illuminate\Support\Collection;

class RportCameras extends JsonApiClient
{
    public function __construct()
    {
        $this->baseUri = env('RPORT_API_URL');
        $this->client = new Client([
            // Base URI is used with relative requests
            'base_uri' => $this->baseUri,
            // You can set any number of default request options.
            'timeout'  => 2.0,
            'auth'     => [env('RPORT_API_USER'), env('RPORT_API_TOKEN')],
        ]);
        $this->uri = 'clients';
        $this->uriSupports = ['get'];
    }


    public function getAll(): Collection
    {
        $fields = ['id', 'name', 'tags', 'hostname', 'connection_state'];
        $this->uri = 'clients?fields[clients]=' . implode(',', $fields);
        $cameras = collect();
        foreach ($this->executeRequest() as $camera) {
            $cameras->push(CameraData::create($camera));
        };

        return $cameras;
    }
}
