<?php

namespace App\Actions;

use App\Clients\RportCamera;
use App\Data\CameraData;

/**
 * Return data of a camera
 */
class GetCameraAction
{
    /**
     * @param string $id
     * @return CameraData
     */
    public function execute(string $id): CameraData
    {
        $camera = new RportCamera($id);

        return $camera->get();
    }
}
