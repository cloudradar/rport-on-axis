<?php

namespace App\Actions;

use App\Clients\RportCamera;
use App\Data\ExposalData;
use App\Jobs\CaddyDeleteRouteJob;
use App\Memory\ExposalMemory;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class UnexposeCameraAction
{
    public function executeFromController(string $cameraId): void
    {
        if (!$exposal = ExposalMemory::get($cameraId)) {
            throw new NotFoundHttpException('Camera exposal does not exist.');
        };
        $this->execute($exposal);
    }

    public function execute(ExposalData $exposal): void
    {
        /**
         * Step 1: Close the rport tunnel, if exists
         */
        $camera = new RportCamera($exposal->cameraId);
        if ($camera->getTunnels()->firstWhere('id', $exposal->tunnelId)) {
            $camera->deleteTunnel($exposal->tunnelId);
        } else {
            Log::debug('Tunnel requested to be deleted does not exist anymore.', ['camera_id' => $exposal->cameraId, 'tunnel_id' => $exposal->tunnelId]);
        }


        /**
         * Step 2: Remove the routing from the caddy server aka destroy the virtual host.
         *         The causes the webserver to jump into the default virtual host that always returns 404.
         */
        dispatch(new CaddyDeleteRouteJob($exposal->exposalId));
        ExposalMemory::delete($exposal->cameraId);
    }
}
