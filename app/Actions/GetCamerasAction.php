<?php

namespace App\Actions;

use App\Clients\RportCameras;
use App\Data\CameraData;
use Illuminate\Support\Collection;

class GetCamerasAction
{
    public function execute(): Collection
    {
        $cameras = new RportCameras();
        return $cameras->getAll();
    }

}
