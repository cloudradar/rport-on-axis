<?php

namespace App\Actions;

use App\Clients\CaddyProxyClient;
use App\Clients\RportCamera;
use App\Data\CaddyProxyRoute;
use App\Data\ExposalData;
use App\Helpers\ExposalStatus;
use App\Jobs\CaddyAddRouteJob;
use App\Memory\ExposalMemory;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ExposeCameraAction
{
    /**
     * Expose the internal webserver of camera to the public internet on a randomly created domain.
     * @param string $cameraId
     * @return string[]
     */
    public function execute(string $cameraId, ?string $cameraUser, ?string $cameraPassword): ExposalData
    {
        Log::debug('Camera exposal request.', ['cameraId' => $cameraId, 'cameraUser' => $cameraUser, 'cameraPassword' => $cameraPassword]);
        /**
         * Step1 : Create the tunnel so the built-in webserver of the camera
         *         becomes available on localhost of the rport server.
         *         Axis runs different apache configuratrions on different ips of localhost.
         */
        $camera = new RportCamera($cameraId);
        $cameraData = $camera->get();
        if ('connected' != $cameraData->connectionState) {
            throw new HttpException(400, 'Camera is not connected');
        }
        $activeTunnelId = null;
        if ($tunnels = collect($cameraData->tunnels)) {
            if ($activeTunnel = $tunnels->firstWhere('rport', 80)) {
                Log::debug('Active tunnels for camera ' . $cameraId, [$activeTunnel]);
                $activeTunnelId = (int)$activeTunnel['id'];
                //throw new HttpException(400, 'Camera has an active tunnel to remote port 80');
            }
        }
        if ($hostname = ExposalStatus::isExposed(ExposalMemory::get($cameraId))) {
            throw new HttpException(400, 'The camera is aready exposed to https://' . $hostname);
        }
        $tunnel = $camera->createTunnel([
            'remote'               => '127.0.0.2:80',
            'scheme'               => 'http',
            'acl'                  => '127.0.0.1',
            'idle-timeout-minutes' => 5,
        ], $activeTunnelId);

        Log::debug('Tunnel created for camera ' . $cameraId, $tunnel->toArray());
        /**
         * Step 2: Grab the randomly assigned local port of the tunnel,
         *         and create a caddy proxy instance using the tunnel as the upstream server.
         */
        $exposalId = Str::uuid();
        $hostname = $exposalId . '.' . env('CAMERAS_BASE_DOMAIN');
        $exposal = new ExposalData(
            exposalId: $exposalId,
            cameraId: $cameraId,
            hostname: $hostname,
            createTimestamp: time(),
            tunnelId: $tunnel->id,
            lport: $tunnel->lport,
            authUsername: $cameraUser ?: env('DEFAULT_CAMERA_USER'),
            authPassword: $cameraPassword ?: env('DEFAULT_CAMERA_PASSWORD'),
        );
        // The caddy config needs to be changed asynchronosly from a process decoupled from the current HTTP request.
        // Caddy waits for all https connections to be finished. If the current PHP script is tied to a caddy http connection
        // we would create a race condition.
        dispatch(new CaddyAddRouteJob($exposal));

        /**
         * Step 3:
         * Create the first heartbeat, otherwise the exposal will be removed by the clean-up job
         */
        Cache::put($exposal->exposalId, time(), 30);

        /**
         * Step 4: Store the details of the exposal into the memory.
         */
        ExposalMemory::store($exposal);

        return $exposal;
    }
}
