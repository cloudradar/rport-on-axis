#!/bin/sh

#
# Installer Script to install the rport client properly on an Axis camera
# https://bitbucket.org/cloudradar/rport-on-axis/src/master/docs/install-on-camera.md
#

# Set RPort Client Version
VERSION="0.5.0"

# Define some functions
abort() {
  echo >&2 "$1 Exit!"
  exit 1
}

# Check variables are set
[ -z $RPORT_SERVER ]&&abort "var RPORT_SERVER undefined"
[ -z $RPORT_CLIENT_ID ]&&abort "var RPORT_CLIENT_ID undefined"
[ -z $RPORT_PASSWORD ]&&abort "var RPORT_PASSWORD undefined"
[ -z $RPORT_FINGERPRINT ]&&abort "var RPORT_FINGERPRINT undefined"

# Create the needed directories
mkdir -p /opt/rport/bin
mkdir -p /opt/rport/etc
mkdir -p /opt/rport/lib
mkdir -p /opt/rport/log
cd /tmp
CONFIG_FILE="/opt/rport/etc/rport.conf"
LOG_FILE="/opt/rport/log/rport.log"
DATA_DIR="/opt/rport/lib"

# Download from github
curl -LO https://github.com/cloudradar-monitoring/rport/releases/download/${VERSION}/rport_${VERSION}_Linux_mipsle_softfloat.tar.gz

# Unpack the binary
tar xzf rport_${VERSION}_Linux_mipsle_softfloat.tar.gz -C /opt/rport/bin/ rport
/opt/rport/bin/rport --version
echo "RPort client installed to /opt/rport/bin/rport"

# Delete the download
rm rport_${VERSION}_Linux_mipsle_softfloat.tar.gz

# Prepare the configuration file
echo "Creating configuration file $CONFIG_FILE"
cat << EOF > "$CONFIG_FILE"
[client]
 server = "${RPORT_SERVER}"
 fingerprint = "${RPORT_FINGERPRINT}"
 auth = "${RPORT_CLIENT_ID}:${RPORT_PASSWORD}"
 data_dir = "${DATA_DIR}"
 id = "$(cat /etc/machine-id)"
 name = "$(hostname)"
 tags = ['Axis']
[connection]
 keep_alive = '30s'
[logging]
 log_level = "error"
 log_file = "${LOG_FILE}"
[remote-commands]
 enabled = false
[remote-scripts]
 enabled = false
[monitoring]
 enabled = false
EOF

chown -R daemon /opt/rport

# For a manual test execute
#su - daemon -s /bin/sh -c "/opt/rport/bin/rport -c /opt/rport/etc/rport.conf"

# Install the service
/opt/rport/bin/rport --service install --service-user daemon -c /opt/rport/etc/rport.conf
systemctl start rport
systemctl status rport
systemctl enable rport
echo "RPort Client successfully installed"
