# Install the RPort client on an Axis camera 

Because remote scripting is not needed you don't need a dedicated user account to run rport.
The default user "daemon" is perfectly suitable.

The [install-on-camers.sh](./install-on-camera.sh) script can be downloaded and executed directly on the camera.

The script expected credentials and connecion details being exported to the shell environment. See example.

## Example
* Log in to the camera via SSH using the root user
* Paste the following lines into the terminal

```bash
export RPORT_SERVER='rport.cameras.tinyserver.net:80'
export RPORT_CLIENT_ID='client1'
export RPORT_PASSWORD='****'
export RPORT_FINGERPRINT='****'
cd /tmp
curl -LO https://bitbucket.org/cloudradar/rport-on-axis/downloads/install-on-camera.sh
sh install-on-camera.sh
rm install-on-camera.sh
```

🎉 **voilà** 
